import 'package:e_commerce/files/constantes.dart';
import 'package:flutter/material.dart';
class ProductItem extends StatelessWidget {
  final String amount;
  final String label;
  const ProductItem({
    Key? key,
    required this.amount,
    required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return   Container(
                      margin: EdgeInsets.symmetric(vertical: 20,horizontal: 35),
                      padding: EdgeInsets.all(12),
                      height: 120,
                      
                      decoration: BoxDecoration(
                        color: Colors.green.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(13),
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 17),
                            blurRadius: 23,
                            spreadRadius: -13,
                            color: Colors.orange.withOpacity(0.1),
                          ),
                        ],
                      ),
                      child: Row(
                        children: <Widget>[
                          Image.asset(
                            "assets/images/shoes.png",
                          ),
                          SizedBox(width: 20),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Vetements",
                                  style: TextStyle(fontWeight: FontWeight.w300,fontSize: 18,color: Colors.orange),
                                ),
                                Text("Chaussure Nike 2506"),
                                Text("250000 FCFA",style: TextStyle(fontWeight: FontWeight.w800),)
                              ],
                            ),
                          ),
                          
                        ],
                      ),
                    );
             
  }
}