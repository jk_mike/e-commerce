import 'package:auto_size_text/auto_size_text.dart';
import 'package:e_commerce/files/constantes.dart';
import 'package:flutter/material.dart';
class categoryItem extends StatelessWidget {
  final String text;
  const categoryItem({
    Key? key,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 5.0, right: 5.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: Container(
          color: kBlueColor,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: AutoSizeText(
              text,
              style: TextStyle(fontSize: 12.0, color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}