
import 'package:flutter/material.dart';
import 'ui/Categories.dart' as screens;
import 'ui/Dashboard.dart' as screens;
import 'ui/ProductDetail.dart' as screens;
import 'ui/SearchResult.dart' as screens;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'E-commerce',
      theme: ThemeData(
     //  scaffoldBackgroundColor: kBackgroundColor,
       // textTheme: Theme.of(context).textTheme.apply(displayColor: kTextColor),
        primarySwatch: Colors.red,
      ),
      home: screens.Dashboard(),
      routes: {
             screens.Dashboard.routeName: (context) =>screens.Dashboard(),
            screens.Categories.routeName: (context) =>screens.Categories(),
            screens.ProductList.routeName: (context) =>screens.ProductList(),
            screens.Detail.routeName: (context) =>screens.Detail(),
            },
    );
  }
}
