import 'package:flutter/painting.dart';

class Article {
  late final String name;
  late final double price;
  late final String desc;
  late final Color color;
  late final String imgPath;
  Article(
      {required this.name,
      required this.color,
      required this.desc,
      required this.imgPath,
      required this.price});

  static List<Article> list = [
    Article(
        name: "Electomenager",
        color: Color(0xFFC7B8F5),
        desc: "accessoire de jeu",
        imgPath: "imgPath",
        price: 25896)
  ];
}
