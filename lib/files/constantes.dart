import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

const String pathImage = "images/";

 const Color secondaryColor = Color(0xFF079585);
const Color background=Color.fromARGB(255,240,243,245);
const Color whitecolor=Colors.white;
const Color blackcolor=Colors.black;
const kBackgroundColor = Color(0xFFF8F8F8); 
const Color primaryColor=Color(0x20894e);
const kActiveIconColor = Color(0xFFE68342);
 const kTextColor = Color(0xFF222B45);
const kBlueLightColor = Color(0xFFC7B8F5);
const kBlueColor = Color(0xFF817DC0);
const kShadowColor = Color(0xFFE6E6E6);
const kSecondaryColor=Color(0xFF979797); 
//fromARGB(255, 177, 231, 239);
//fromRGBO(241,243,255,1);

const TextStyle textStyle1=TextStyle(fontSize: 25,fontWeight:FontWeight.bold);
const TextStyle textStyle2=TextStyle(fontSize: 18,fontWeight:FontWeight.bold);
const TextStyle textStyle3=TextStyle(fontSize: 18,color: Colors.white,);
