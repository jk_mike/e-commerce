import 'package:auto_size_text/auto_size_text.dart';
import 'package:e_commerce/Models/Article.dart';
import 'package:e_commerce/files/constantes.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  static const routeName = "/home";
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Article> lis = Article.list;
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange.withOpacity(0.8),
        elevation: 0,
        automaticallyImplyLeading: false,
        title: Text(
          "App Name",
          style: textStyle2,
        ),
        centerTitle: true,
        actions: [Icon(Icons.shopping_cart)],
      ),
      body: ListView(
        children: [
                     Container(
              height: 50,
              color:Colors.green.shade100,
              child: Row(
mainAxisAlignment: MainAxisAlignment.spaceBetween,
crossAxisAlignment: CrossAxisAlignment.center,
children: [
  ActionButton(icondata: Icons.favorite, label: 'Mes favoris',),
  Container(width: 2,height: 20,color: Colors.amber,),
  ActionButton(icondata: Icons.person, label: 'profil',),
  Container(width: 2,height: 20,color: Colors.amber,),
  ActionButton(icondata: Icons.settings, label: 'Parametres',),
  Container(width: 2,height: 20,color: Colors.amber,)
],
              ),
            ), 
          Column(
            children: [
             SizedBox(height: 10,),
              Column(
                children: [
                  Text(
                    "Bienvuenue sur App Name",
                    style: TextStyle(
                        fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "faites vos achats en ligne ici",
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  )
                ],
              ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0,vertical: 10),
                    child: TextField(decoration: InputDecoration(
                      hintText: "Rechercher un produit",
                      prefixIcon: Icon(Icons.search,size: 30,),
                      filled: true,
                      fillColor: Colors.grey.shade200,
                      contentPadding: EdgeInsets.all(15),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15),
                        borderSide: BorderSide(color: Colors.grey.shade200)
                      ),
                       focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15),
                        borderSide: BorderSide(color: Colors.grey.shade200)
                      ),
                      
                    ),),
                  ),
                ),
              GestureDetector(
                onTap: (){
                   Navigator.of(context).pushNamed("/result");
                },
                child: Container(
                  margin: EdgeInsets.only(left: 10,right: 10),
                  padding: EdgeInsets.all(10 ),
                  decoration: BoxDecoration(color: Colors.orange,borderRadius: BorderRadius.circular(15)),
                  child: RotatedBox(quarterTurns: 0,child: Text("ok",style: TextStyle(fontSize: 15),))),
              ),
              
              ],
            ),

Stack(
              children: [
                Container(
                  height: size.height * .25,
                  decoration: BoxDecoration(
                    color: kBlueLightColor,
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 2.0, right: 10),
                    child: Container(
                      // alignment: Alignment.centerLeft,
                      height: size.height * 0.15,
                      child: Image.asset("assets/images/meditation_bg.png"),
                    ),
                  ),
                ),
                Align(
                    alignment: Alignment.centerRight,
                    child: SafeArea(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: size.height * 0.05,
                              ),
                              Text(
                                "Application",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 10),
                              SizedBox(
                                width: size.width *
                                    .6, // it just take 60% of total width
                                child: Text(
                                  "Faites vos achats en ligne et faites vous livrer en un temps record",
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(vertical: 25),
                                padding: EdgeInsets.all(15),
                                height: 90,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(13),
                                  boxShadow: [
                                    BoxShadow(
                                      offset: Offset(0, 17),
                                      blurRadius: 23,
                                      spreadRadius: -13,
                                      color: kShadowColor,
                                    ),
                                  ],
                                ),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Center(
                                        child: Text(
                                          "DANGOTE KOSSI",
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline5,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ))
              ],
            ),
           // SizedBox(height: 10,),
          
          Container(
            height:size.height/7,
            width: double.infinity,
            
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Container(
                    width: size.width/4,height:size.height,
                    color: Colors.green.shade300,
                    child: Center(child: Text("Tout"),),
                  ),
                ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.all(5),
                                color: Colors.white,
                              width: double.infinity,
                                child: GridView.count(
                        crossAxisCount: 2,
                        childAspectRatio: 3,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                        physics:BouncingScrollPhysics(),
                        //padding: EdgeInsets.all(.0),
                        children:[
                          ClipRRect(
                            borderRadius: BorderRadius.circular(15),
                            child: Container(
                              color: kBlueColor,
                              child: Center(
                                child: AutoSizeText(
                                  "Vetements",
                                  style: TextStyle(fontSize: 12.0, color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                          ClipRRect(
                            borderRadius: BorderRadius.circular(15),
                            child: Container(
                              color: kBlueColor,
                              child: Center(
                                child: AutoSizeText(
                                  "Vetements",
                                  style: TextStyle(fontSize: 12.0, color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                          ClipRRect(
                            borderRadius: BorderRadius.circular(15),
                            child: Container(
                              color: kBlueColor,
                              child: Center(
                                child: AutoSizeText(
                                  "Vetements",
                                  style: TextStyle(fontSize: 12.0, color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                          ClipRRect(
                            borderRadius: BorderRadius.circular(15),
                            child: Container(
                              color: kBlueColor,
                              child: Center(
                                child: AutoSizeText(
                                  "Vetements",
                                  style: TextStyle(fontSize: 12.0, color: Colors.white),
                                ),
                              ),
                            ),
                          )
                        ])),
                  )
              ],
            ),
          ),
           Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Nouveautes",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    Text(
                      "Tout voir",
                      style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                          fontSize: 15),
                    ),
                  ]),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(
                          vertical: 10, horizontal: 15),
                      padding: EdgeInsets.all(5),
                      height: 120,
                      decoration: BoxDecoration(
                        color: Colors.red.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(13),
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 17),
                            blurRadius: 23,
                            spreadRadius: -13,
                            color: Colors.orange.withOpacity(0.1),
                          ),
                        ],
                      ),
                      child: Container(
                        width: size.width * .8,
                        child: Row(
                          children: [
                            Image.asset(
                              "assets/images/shoes.png",
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Vetements",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w300,
                                            fontSize: 18,
                                            color: Colors.orange),
                                      ),
                                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 5,vertical: 2),
                        decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(10.0)),
                            child: Text("promotion",style: TextStyle(color: Colors.white,fontSize: 15.0,height: 1.5),),
                      ),
                                    ],
                                  ),
                                  Text("Chaussure Nike 2506"),
                                  Text(
                                    "250000 FCFA",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        decoration:
                                            TextDecoration.lineThrough),
                                  ),
                                  Text(
                                    "150000 FCFA",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w800),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(
                          vertical: 10, horizontal: 15),
                      padding: EdgeInsets.all(5),
                      height: 120,
                      decoration: BoxDecoration(
                        color: Colors.green.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(13),
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 17),
                            blurRadius: 23,
                            spreadRadius: -13,
                            color: Colors.orange.withOpacity(0.1),
                          ),
                        ],
                      ),
                      child: Container(
                        width: size.width * .8,
                        child: Row(
                          children: [
                            Image.asset(
                              "assets/images/shoes.png",
                            ),
                            SizedBox(width: 5),
                            Expanded(
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Vetements",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w300,
                                            fontSize: 18,
                                            color: Colors.orange),
                                      ),
                                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 5,vertical: 2),
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(10.0)),
                            child: Text("Nouveau",style: TextStyle(color: Colors.white,fontSize: 15.0,height: 1.5),),
                      ),
                                    ],
                                  ),
                                  Text("Chaussure Nike 2506"),
                                  Text(
                                    "250000 FCFA",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w800),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Les produits",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    Text(
                      "Tout voir",
                      style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                          fontSize: 15),
                    ),
                  ]),
              SizedBox(
                height: 10,
              ),
             
            Container(
              color: Colors.white,
            height: size.height * 0.5,
              child: GridView.count(
      crossAxisCount: 2,
      childAspectRatio: 1,
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      physics:BouncingScrollPhysics(),
      //padding: EdgeInsets.all(.0),
      children:[
       Container(
        
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.grey.shade200
                  ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Stack(
                        children: [
                           Stack(
                             children: [
                               Center(child: Image.asset("assets/images/shoes.png",width: 200,fit: BoxFit.cover ,)),
                             ],
                           ),
                          Align(
                            alignment: Alignment.topRight,
                            child: Container(
                              padding: EdgeInsets.all(6),
                              decoration: BoxDecoration(borderRadius:  BorderRadius.circular(50),color: Colors.pink.shade100,),
                              child: Icon(Icons.favorite),
                            ),
                          )
                        ],
                      ),
                     
                    ),
                     SizedBox( height: 8,),
                     Text("chaussure en vente",style: TextStyle(fontWeight: FontWeight.bold),),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,

                       children: [
                         Text("258000 FCFA"),
                         Container(
                           padding: EdgeInsets.all(4),
                           decoration: BoxDecoration(borderRadius: BorderRadius.circular(8),
  color: Colors.grey.shade400
                           ),
                           child: Icon(Icons.add),
                         )
                       ],
                     )
                  ],
                ),
                ),
                  Container(
        
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.grey.shade200
                  ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Stack(
                        children: [
                           Stack(
                             children: [
                               Center(child: Image.asset("assets/images/shoes.png",width: 200,fit: BoxFit.cover ,)),
                             ],
                           ),
                          Align(
                            alignment: Alignment.topRight,
                            child: Container(
                              padding: EdgeInsets.all(6),
                              decoration: BoxDecoration(borderRadius:  BorderRadius.circular(50),color: Colors.pink.shade100,),
                              child: Icon(Icons.favorite),
                            ),
                          )
                        ],
                      ),
                     
                    ),
                     SizedBox( height: 8,),
                     Text("chaussure en vente",style: TextStyle(fontWeight: FontWeight.bold),),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,

                       children: [
                         Text("258000 FCFA"),
                         Container(
                           padding: EdgeInsets.all(4),
                           decoration: BoxDecoration(borderRadius: BorderRadius.circular(8),
  color: Colors.grey.shade400
                           ),
                           child: Icon(Icons.add),
                         )
                       ],
                     )
                  ],
                ),
                ),
                  Container(
        
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.grey.shade200
                  ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Stack(
                        children: [
                           Stack(
                             children: [
                               Center(child: Image.asset("assets/images/shoes.png",width: 200,fit: BoxFit.cover ,)),
                             ],
                           ),
                          Align(
                            alignment: Alignment.topRight,
                            child: Container(
                              padding: EdgeInsets.all(6),
                              decoration: BoxDecoration(borderRadius:  BorderRadius.circular(50),color: Colors.pink.shade100,),
                              child: Icon(Icons.favorite),
                            ),
                          )
                        ],
                      ),
                     
                    ),
                     SizedBox( height: 8,),
                     Text("chaussure en vente",style: TextStyle(fontWeight: FontWeight.bold),),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,

                       children: [
                         Text("258000 FCFA"),
                         Container(
                           padding: EdgeInsets.all(4),
                           decoration: BoxDecoration(borderRadius: BorderRadius.circular(8),
  color: Colors.grey.shade400
                           ),
                           child: Icon(Icons.add),
                         )
                       ],
                     )
                  ],
                ),
                ),
                  Container(
        
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.grey.shade200
                  ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Stack(
                        children: [
                           Stack(
                             children: [
                               Center(child: Image.asset("assets/images/shoes.png",width: 200,fit: BoxFit.cover ,)),
                             ],
                           ),
                          Align(
                            alignment: Alignment.topRight,
                            child: Container(
                              padding: EdgeInsets.all(6),
                              decoration: BoxDecoration(borderRadius:  BorderRadius.circular(50),color: Colors.pink.shade100,),
                              child: Icon(Icons.favorite),
                            ),
                          )
                        ],
                      ),
                     
                    ),
                     SizedBox( height: 8,),
                     Text("chaussure en vente",style: TextStyle(fontWeight: FontWeight.bold),),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,

                       children: [
                         Text("258000 FCFA"),
                         Container(
                           padding: EdgeInsets.all(4),
                           decoration: BoxDecoration(borderRadius: BorderRadius.circular(8),
  color: Colors.grey.shade400
                           ),
                           child: Icon(Icons.add),
                         )
                       ],
                     )
                  ],
                ),
                )
      ]
    ),
            )
              //item1()
             
            ],
          ),
        ],
      ),
    );
  }
}

class ActionButton extends StatelessWidget {
  final String  label;
  final IconData icondata;
  const ActionButton({
    Key? key,  required this.label,required this.icondata,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(onTap: (){

    },
    child: Wrap(
      crossAxisAlignment: WrapCrossAlignment.center,
      spacing: 10.0,
      children: [
        Icon(icondata,color: Colors.green,),
        Stack(
          overflow: Overflow.visible,
          children: [Text(label,style: TextStyle(fontSize: 16.0,fontWeight: FontWeight.bold),)],
        )
      ],
    ),
    );
  }
}

class item1 extends StatelessWidget {
  const item1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(6),
      width: 300,
      height: 160,
      decoration: BoxDecoration(
        color: Colors.orange.withOpacity(0.2),
        borderRadius: BorderRadius.circular(7.0),
        boxShadow: [
          BoxShadow(
              color: Colors.black.withOpacity(0.1),
              offset: Offset.zero,
              blurRadius: 15.0)
        ],
        // gradient: LinearGradient(begin: Alignment.topRight,end: Alignment.bottomLeft,colors: [secondaryColor,primaryColor,])
      ),
      child: Stack(
        children: [
           Align(
          alignment: Alignment.topRight,
           
              child: Image.asset("assets/images/shoes.png")),
          Wrap(
            direction: Axis.vertical,
            children: [
              SizedBox(height: 10,),
              Text(
                "Electromenager",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    height: 1.5),
              ),
              Text(
                "chaussure de sport",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                    height: 1.5),
              ),
              Text(
                "25000 FCFA",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 15.0,
                    height: 1.5),
              )
            ],
          ),
         
          Positioned(
            left: 10,
            bottom: 20,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 5,vertical: 2),
              decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.circular(10.0)),
                  child: Text("Nouveau",style: TextStyle(color: Colors.white,fontSize: 15.0,height: 1.5),),
            ),
            
          )
        ],
      ),
    );
  }
}
