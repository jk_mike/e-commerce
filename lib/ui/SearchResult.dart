import 'dart:ui';

import 'package:e_commerce/files/constantes.dart';
import 'package:flutter/material.dart';

class ProductList extends StatelessWidget {
static const routeName = "/result";
  nested(){
    return NestedScrollView(
      headerSliverBuilder: (BuildContext context,bool inner){
      Size size = MediaQuery.of(context).size;
      return <Widget>[
        SliverAppBar(expandedHeight: 150,floating:false,pinned: true,
        backgroundColor: Colors.green,
        flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          title: Text("Chaussures",style: textStyle1),
        ),)];

    }, body:ListView.separated(
 separatorBuilder:(context,index)=>Divider(),
  itemCount: 20,
  itemBuilder: (context, index) => Container(
                     // margin: EdgeInsets.symmetric(vertical: 20),
                      padding: EdgeInsets.all(5),
                      height: 100,
                     /*  decoration: BoxDecoration(
                       // color: Colors.white,
                        borderRadius: BorderRadius.circular(13),
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 17),
                            blurRadius: 23,
                            spreadRadius: -13,
                            color: kShadowColor,
                          ),
                        ],
                      ), */
                      child: Row(
                        children: <Widget>[
                          GestureDetector(
                            onTap: (){
                               Navigator.of(context).pushNamed("/details");
                            },
                            child: Image.asset(
                              "assets/images/shoes.png",
                            ),
                          ),
                          SizedBox(width: 20),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Chaussure de sport",
                                  style: TextStyle(fontStyle: FontStyle.italic,fontSize: 20),
                                ),
                                SizedBox(height: 10,),
                                 Text(
                                  "25 000 FCFA",
                                  style: Theme.of(context).textTheme.subtitle,
                                ),
                               SizedBox(height: 5,),
                                Expanded(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                     ClipRRect(
                                       borderRadius: BorderRadius.circular(10),
                                       child: Container(
                                         padding: EdgeInsets.all(3),
                                         color: Colors.amber,
                                         child: Row(
                                           children: [
                                              Icon(Icons.shopping_cart,size: 30,color: Colors.white,),
                                         Text("ajouter au panier",style: TextStyle(color: Colors.white))
                                           ],
                                         ),
                                       ),
                                     ),
                                     CircleAvatar(child: Icon(Icons.favorite,size: 25,color: Colors.white,),radius: 18,backgroundColor: Colors.green)
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        
                        ],
                      ),
                    )
));
  }
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      body: Container(
       // color: Colors.orange.withOpacity(0.1),
        child: nested(),)
    );
  }
}