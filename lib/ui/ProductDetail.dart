import 'dart:ui';

import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:carousel_pro/carousel_pro.dart';

class Detail extends StatelessWidget {
  static const routeName = "/details";
  const Detail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget Carrousel = new Container(
        height: 220,
        width: MediaQuery.of(context).size.width,
        child: Carousel(
          images: [
            NetworkImage(
                'https://cdn-images-1.medium.com/max/2000/1*GqdzzfB_BHorv7V2NV7Jgg.jpeg'),
            NetworkImage(
                'https://cdn-images-1.medium.com/max/2000/1*wnIEgP1gNMrK5gZU7QS0-A.jpeg'),
          ],
        ));

    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      body: ListView(
        children: [
          SafeArea(
            child: Container(
              height: 280,
              child: GridTile(
                child: Carrousel,
                footer: Container(
                  color: Colors.white70,
                  child: ListTile(
                    leading: Text("Voiture d'occasion"),
                    trailing: Text("250000 FCFA",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20)),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                          text: "Categorie :  ",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 20,
                              color: Colors.green),
                          children: [
                            TextSpan(
                              text: "Electromenager",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 15,
                                  color: Colors.black),
                            ),
                          ]),
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "Description",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "Je suis un produit vraiment unique.Il y a pas mon deux.cherchez moi et vous me trouverez.Je ne suis disponible que sur ce systeme,faites des reservations et abonnez vous.",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
                )
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                  child: MaterialButton(
                onPressed: () {},
                color: Colors.amber.shade700,
                textColor: Colors.white,
                elevation: 0.2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.shopping_cart,
                      color: Colors.white,
                      size: 20,
                    ),
                    Text(
                      "Ajouter au panier",
                      style: TextStyle(fontSize: 20),
                    ),
                  ],
                ),
              )),
              SizedBox(
                width: 10,
              ),
              Icon(
                Icons.favorite,
                color: Colors.green,
                size: 40,
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 60,
              width: double.infinity,
              child: Card(
                color: Colors.grey.shade400,
                elevation: 2,
                child: Center(
                  child: ListTile(
                    title: Text("Voir les produits de cette categorie",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                    trailing: Icon(Icons.arrow_forward_ios),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:8.0),
            child: Container(
              height: 60,
              width: double.infinity,
              child: Card(
                color: Colors.grey.shade400,
                elevation: 2,
                child: Center(
                  child: ListTile(
                    title: Text("Consulter le profil du vendeur",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                    trailing: Icon(Icons.arrow_forward_ios),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
