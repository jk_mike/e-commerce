
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/services.dart';

class Connexion{

static Future<String> isConnected()async{
 String result="false";
  try {
        var connectivityResult = await (Connectivity().checkConnectivity());
        if (connectivityResult == ConnectivityResult.none) 
        {
         result="false";
        }
        else{
         result= "true";
        }
      } on PlatformException catch (e) {
        print(e.toString());
      }
  return result;
}
}