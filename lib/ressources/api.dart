import 'dart:convert';
import 'package:http/http.dart' as http;

const String serverUrl = "https://ebillet.net/api/v1";
const Map<String, String> headers = {
  "Content-type": "application/json",
  "Accept": "application/json",
};

Map<String, String> headersBearer(String token) {
  return {
    "Content-type": "application/json",
    "Accept": "application/json",
    'Authorization': 'Bearer $token',
  };
}

Uri fullUri(String uri){
return Uri.parse('$serverUrl/$uri');
}

class Api {
  static Future<Map> register(

    String name,
    String password,
    String username,
    String email,
    String telephone,
  
  ) async {
    try {
      final response = await http.post(fullUri("register"),
          headers: headers,
          body: json.encode({
            "name": name +""+username,
            "telephone":"228"+telephone,
            "username":"228"+telephone,
            "email": email,
            "password": password,
            
          }));
      final data = json.decode(response.body) as Map<String, dynamic>;
      //print("hello",data.toString());
      return data;
    } catch (err) {
      throw err;
    }
  }

   static Future<Map<String, dynamic>> getTicket(
      String agence,
  String villeDepart,
     String villearriver,
    String datedepart,
    String place ,
    String poids,
    String token
  ) async {
    try {
      //print("helldfkdlfkdlo");
      final response = await http.post(fullUri("ticket/findAgence"),
           headers: headersBearer(token),
          body: json.encode({
            "agence":agence,
            "villedepart": villeDepart,
            "villearriver":villearriver,
            "datedepart":datedepart,
            "place": place,
            "poids": poids,
          }));
      final data = json.decode(response.body) as Map<String, dynamic>;
      
      return data;
    } catch (err) {
      throw err;
    }
  }
    static Future<Map<String, dynamic>> getAgency(

  String villeDepart,
     String villearriver,
    String datedepart,
    String place ,
    String poids,
    String token
  ) async {
    try {
      //print("helldfkdlfkdlo");
      final response = await http.post(fullUri("ticket/find"),
           headers: headersBearer(token),
          body: json.encode({
            "villedepart": villeDepart,
            "villearriver":villearriver,
            "datedepart":datedepart,
            "place": place,
            "poids": poids,
          }));
      final data = json.decode(response.body) as Map<String, dynamic>;
      
      return data;
    } catch (err) {
      throw err;
    }
  }
      static Future<Map<String, dynamic>> madeTransaction(

     String operator,
    String ticketNumber,
    String receiverPhone ,
    String ticketId,
    String nom,
    String token
  ) async {
    try {
      //print("helldfkdlfkdlo");
      final response = await http.post(fullUri("transactionUser/add",),
          headers: headersBearer(token),
          body: json.encode({
            "name": nom,
            "nombre_Ticket":ticketNumber,
            "numeroDestinataire":receiverPhone,
            "operateur": operator,
            "ticket_id": ticketId,
            
          }));
      final data = json.decode(response.body) as Map<String, dynamic>;
    
      return data;
    } catch (err) {
      throw err;
    }
  }

  static Future<Map> login(String number, String password) async {
    try {
     
      final response = await http.post(fullUri("login"),
          headers: headers,
          body: json.encode({
            "name":"228$number",
            "password": password,
          }));
      final data = json.decode(response.body) as Map<String, dynamic>;
      return data;
    } catch (err) {
      throw err;
    }
  } 

  static Future<Map> operatorInfo(String operator,String token) async {
    
    try {
      final response = await http.post(fullUri("operateur/findMobileMoney"),
          headers: headersBearer(token),
          body: json.encode({
            "name": operator,
          }));
      final data = json.decode(response.body) as Map<String, dynamic>;
      return data;
    } catch (err) {
      throw err;
    }
  }
    static Future<Map> deconnexion(String token) async {

    try {
      final response = await http.post(fullUri("logout"),
          headers: headersBearer(token),
);
      final data = json.decode(response.body) as Map<String, dynamic>;
      return data;
    } catch (err) {
      throw err;
    }
  }
}
