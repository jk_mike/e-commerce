import 'package:e_commerce/ressources/api.dart';
import 'package:e_commerce/ressources/shared_preferences.dart' as resources;
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class Auth with ChangeNotifier{

bool registrering=false;
var role;
var _token;
var _name;
var _number;
isregistrering(){
  registrering= !registrering;
}
getData(token,name,phone,rola)async{

  await resources.SharedPreferencesClass.save(
          "token",
          token,
        );
   await resources.SharedPreferencesClass.save(
          "role",
         role,
        );
        await resources.SharedPreferencesClass.save(
          "name",
          name,
        );
        await resources.SharedPreferencesClass.save(
          "number",
          phone,
        );
  notifyListeners();
}

deleteUser() async {
    await resources.SharedPreferencesClass.clear("role");
    await resources.SharedPreferencesClass.clear("name");
    await resources.SharedPreferencesClass.clear("token");
    await resources.SharedPreferencesClass.clear("number");
  }
  initValue() async {
    await resources.SharedPreferencesClass.restore("role").then((value) {
      if (value is String) {
        role = value;
      }
    });
    await resources.SharedPreferencesClass.restore("name").then((value) {
      if (value is String) {
        _name = value;
      }
    });

    await resources.SharedPreferencesClass.restore("token").then((value) {
      if (value is String) {
        _token = value;
      }
    });

    await resources.SharedPreferencesClass.restore("number").then((value) {
      if (value is String) {
        _number = value;
      }
    });
  notifyListeners();
  }

   Future<Map> login(
    String name,
    String password,
   // BuildContext context,
  ) async {
    try {
      final response = await Api.login(name, password);
     print(response);
      String validate = response["status"];
      if (validate.toLowerCase() == "success") {
        final data = response["data"];
        print(data['token']);
        getData(data['token'],data['information']['name'],data['information']['telephone'],data['role']);
        //print(data['role']);
        notifyListeners();
        return data;
      } else if (validate.toLowerCase() == "error") {
        notifyListeners();
        return  {};
      } else {
        notifyListeners();
        return {};
      }
    } catch (err) {
      throw err;
    }
  }

   Future<dynamic> register(
    String name,
     String password,
    String username,
    String email,
    String telephone,
  
   // BuildContext context,
  ) async {
    try {
      final response = await Api.register(name, password,username,email,telephone);
      
     print(response);
      String validate = response["status"];
      if (validate.toLowerCase() == "success") {
        final data = response["data"];
        print(data);
        getData(data['token'],data['information']['name'],data['information']['telephone'],data['role']);
        notifyListeners();
        return "success";
      } else if (validate.toLowerCase() == "error") {
        notifyListeners();
        return response;
      } else {
        notifyListeners();
        return "error";
      }
    } catch (err) {
      throw err;
    }
  }

    Future<String> deconnexion() async {
        final response = await Api.deconnexion(_token);
        print(response);
         String validate = response["status"];
      if (validate.toLowerCase() == "success")
      { 
         deleteUser();
        _name="";
        role="";
        _number="";
        _token="";
          notifyListeners();
      return  "success";
      }else if (validate.toLowerCase() == "error") {
        notifyListeners();
        return  "error";
      } else {
        notifyListeners();
        return "error";
      }
   
   }
}